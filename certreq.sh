#!/bin/bash
#Author: Oleksii Rud

CA_EXTRA="/usr/local/share/ca-certificates/extra"
ROOT_CA_CERT="ca.cer"
CA_SERVER="ca.domain.local"
DOMAIN="DOMAIN"
TEMPLATE="User"

curl -i http://$CA_SERVER/rootca.cer -o $HOME/$ROOT_CA_CERT

if [ ! -d "$CA_EXTRA" ]; then
    sudo mkdir $CA_EXTRA
fi
if [ ! -f "$CA_EXTRA/$ROOT_CA_CERT" ]; then
    sudo cp $HOME/$ROOT_CA_CERT $CA_EXTRA/$ROOT_CA_CERT
    sudo update-ca-certificates
fi

read -p Username: Username
read -sp Password: Password

echo -e "\e[32m1. Generate private key...\e[0m"
openssl req -new -nodes -out $Username.csr -keyout $Username.key -subj "/C=DE/ST=State/L=City/O=Org/CN=$Username/emailAddress=postmaster@example.com"
CERT=`cat $Username.csr | tr -d '\n\r'`
CERT=`echo ${CERT} | sed 's/+/%2B/g'`
CERT=`echo ${CERT} | tr -s ' ' '+'`
CERTATTRIB="CertificateTemplate:$TEMPLATE%0D%0A"

echo -e "\e[32m2. Request cert...\e[0m"
RESPONCE=$(curl -i -k -u ${DOMAIN}\\${Username}:${Password} --ntlm --http1.1 \
"https://${CA_SERVER}/certsrv/certfnsh.asp" \
-H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' \
-H 'Accept-Encoding: gzip, deflate' \
-H 'Accept-Language: en-US,en;q=0.5' \
-H 'Connection: keep-alive' \
-H "Host: ${CA_SERVER}" \
-H "Referer: https://${CA_SERVER}/certsrv/certrqxt.asp" \
-H 'User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko' \
-H 'Content-Type: application/x-www-form-urlencoded' \
--data "Mode=newreq&CertRequest=${CERT}&CertAttrib=${CERTATTRIB}&TargetStoreFlags=0&SaveCert=yes&ThumbPrint=")

STATUSCODE=$(echo "$RESPONCE" | sed -E -n 's/HTTP\/[0-9\.]+[ ]([0-9]+)[ ]+(.*)/\1/pg' | tail -n 1)
STATUSDESC=$(echo "$RESPONCE" | sed -E -n 's/HTTP\/[0-9\.]+[ ]([0-9]+)[ ]+(.*)/\2/pg' | tail -n 1)

if [ "200" -ne "$STATUSCODE" ] ; then
    echo "Failed to submit request to CA. HTTP Status: ${STATUSCODE} ${STATUSDESC}"
    echo "$RESPONCE"
    exit 1
fi

OUTPUTLINK=$(echo "$RESPONCE" | sed -E -n 's/.*location=\"(certnew\.cer\?ReqID=[0-9]+\&).*/\1/p')
 if [ "" -eq "$OUTPUTLINK" ] ; then
    echo "Couldn't get certificate download link. Unexpected server responce"
    echo "$RESPONCE"
    exit 1
 fi

CERTLINK="https://${CA_SERVER}/certsrv/${OUTPUTLINK}b64"

echo -e "\e[32m3. Retrive cert: $CERTLINK\e[0m"
curl -i -k -u ${DOMAIN}\\${Username}:${Password} --http1.1 --ntlm $CERTLINK \
-H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' \
-H 'Accept-Encoding: gzip, deflate' \
-H 'Accept-Language: en-US,en;q=0.5' \
-H 'Connection: keep-alive' \
-H "Host: ${CA_SERVER}" \
-H "Referer: https://${CA_SERVER}/certsrv/certrqxt.asp" \
-H 'User-Agent: Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko' \
-H 'Content-Type: application/x-www-form-urlencoded' > $Username.crt

echo -e "\e[32m4. Verifying cert for $Username\e[0m"
openssl verify -CAfile ${HOME}/${ROOT_CA_CERT} -verbose $Username.crt
if [ "0" -eq "$?" ] ;
    then
        openssl pkcs12 -export -out $Username.pfx -inkey $Username.key -in $Username.crt -CAfile ${HOME}/${ROOT_CA_CERT}
        rm $Username.key
        rm $Username.crt
        rm $Username.csr
        echo -e "\e[32mCertificate request succeded. Resulting file is ${Username}.pfx  \e[0m"
        exit 0
    else
        echo -e "\e[31;47mSorry, something went wrong. Try again. Error code: $?.\e[0m"
        exit 1
fi